<?php

/**
 * @file
 * Transliterate File Names.
 *
 */

$plugin = array(
  'form' => 'feeds_tamper_transliterate_form',
  'callback' => 'feeds_tamper_transliterate_callback',
  'name' => 'Transliterate File Name',
  'category' => 'Text',
);

function feeds_tamper_transliterate_form($importer, $element_key, $settings) {
  $form = array();
  $form['info'] = array(
    '#markup' => t('Transliterates and sanitizes a file name. The resulting
    file name has white space replaced with underscores, consists of only
    US-ASCII characters, and is converted to lowercase (if configured).</p>'),
  );
  $form['langcode'] = array(
    '#type' => 'textfield',
    '#title' => t('Language code'),
    '#default_value' => isset($settings['langcode']) ? $settings['langcode'] : NULL,
    '#description' => t('Optional ISO 639 language code that denotes the language
    of the input and is used to apply language-specific variations. If the source
    language is not known at the time of transliteration, it is recommended to set 
    this argument to the site default language to produce consistent results. 
    Otherwise the current display language will be used.'),
  );
  return $form;
}

function feeds_tamper_transliterate_callback($result, $item_key, $element_key, &$field, $settings, $source) {
    $field = transliteration_clean_filename($field, $settings['langcode']);
}
